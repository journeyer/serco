provider "digitalocean" {
  token = "${var.token}"
}

resource "digitalocean_droplet" "app-rest-api" {
  count = 1
  image = "ubuntu-18-04-x64"
  name = "app-rest-api"
  region = "${var.region}"
  size = "s-1vcpu-1gb"
  ipv6 = true
  "private_networking" = true
  ssh_keys = ["${var.ssh_fingerprint}"]

  connection {
      user = "root"
      type = "ssh"
      private_key = "${var.pvt_key}"
      timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      # install nginx
      "apt-get update",
      "DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y --force-yes ",
      "DEBIAN_FRONTEND=noninteractive apt-get -y --force-yes install wget curl python nginx openjdk-11-jdk"
    ]
  }

  provisioner "file" {
    source      = "nginx_default"
    destination = "/etc/nginx/sites-enabled/default"
  }

  provisioner "file" {
    source      = "install_start_metabase.sh"
    destination = "/tmp/install_start_metabase.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/install_start_metabase.sh",
      "/tmp/install_start_metabase.sh",
    ]
  }
}

resource "digitalocean_domain" "default" {
   name = "dev0718.do.chakradeo.net"
   ip_address = "${digitalocean_droplet.app-rest-api.ipv4_address}"
}

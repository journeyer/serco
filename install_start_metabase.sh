#!/bin/sh

apt install -y docker.io
docker run -d -p 3000:3000 --name metabase metabase/metabase

service nginx reload


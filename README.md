This is a terraform project to create a droplet on digital ocean and download and configure an instance of metabase on it

* Install terraform
* Change variables.tf and add the Digital Ocean API Access Token
* Issue command `terraform init` to initialize modules.
* Issue command `terraform plan` to see what resources will get created
* Issue command `terraform apply` to create the cloud resources.
* It takes about 5 minutes for the server to be created and configured and about 5 minutes after that for metabase to initialize itself and the databases.
* Browse to the IP address of the server to access metabase web admin interface.



